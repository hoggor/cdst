shopt -s histappend
HISTSIZE=4400
HISTFILESIZE=22000
shopt -s checkwinsize
alias ll=`ls -l`
alias la=`ls -la`
export PS1="\[\e[00;31m\]\u\[\e[0m\]\[\e[01;31m\]::continent\[\e[0m\]\[\e[00;37m\] > \[\e[0m\]\[\e[01;32m\]\w\[\e[0m\]\[\e[00;37m\]\n\[\e[0m\]\[\e[01;32m\]\\$\[\e[0m\]"
export PATH="/bin/:/usr/bin"