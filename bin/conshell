#! /bin/sh

#############################################
###   Continent Shell Installer           ###
###                                       ###
###   03 2015, k.kodanev@securitycode.ru  ###
###   This file is released under GPL-3   ###
###              or later.                ###
#############################################

## Some variables ##
ACTION=$1
ASK="null"

#### Target ####
DEV_ETC=/dev/ada0s1d
DEV_ROOT=/dev/ada0s1a

#### DIR ####
DST_DIR=/mnt
BS_DIR=/usr/share/bootstrap

#### Utils #####
FSCK=/sbin/fsck_ufs
MOUNT=/sbin/mount
UMOUNT=/sbin/umount
LINKER=/bin/ln
SYNC=/bin/sync
RM=/bin/rm
CP=/bin/cp
AWK=/usr/bin/awk
bootstrap ()
{
if [ -d "$DST_DIR"/bin ]
  then
    echo "Found /bin dir. Copy files to destination $DST_DIR/bin"
    cd "$DST_DIR"/bin
    $CP -vvr "$BS_DIR"/bin/* .
    if [ $? == 0 ]
      then
        echo "Successfully copy to /bin"
      else
        echo "Can't copy /bin files!"
        exit 1
    fi
    cd "$DST_DIR"
  else
    echo "Can't find /bin dir!"
    exit 1
fi

if [ -d "$DST_DIR"/etc ]
  then
    echo "Found /etc dir. Copy files to destination $DST_DIR/etc"
    cd "$DST_DIR"/etc
    $CP -vr "$BS_DIR"/etc/* .
    if [ $? == 0 ]
      then
        echo "Successfully copy to /etc"
      else
        echo "Can't copy /etc files!"
        exit 1
    fi
        cd "$DST_DIR"
  else
    echo "Can't find /etc dir!"
    exit 1
fi

if [ -d "$DST_DIR"/usr ]
  then
    echo "Found /usr dir. Copy files to destination $DST_DIR/usr"
    cd "$DST_DIR"/usr
    $CP -vr "$BS_DIR"/usr/* .
    if [ $? == 0 ]
      then
        echo "Successfully copy to /usr"
      else
        echo "Can't copy /usr files!"
        exit 1
    fi
        cd "$DST_DIR"
  else
    echo "Can't find /usr dir! Creating..."
    cd "$DST_DIR"
    $CP -vr "$BS_DIR"/usr .
    if [ $? == 0 ]
      then
        echo "Successfully copy to /usr"
      else
        echo "Can't copy /usr files!"
        exit 1
    fi
        cd "$DST_DIR"
fi

if [ -d "$DST_DIR"/root/ ]
  then
    echo "Found /root dir."
    cd "$DST_DIR"
    $CP -vr "$BS_DIR"/root .
    if [ $? == 0 ]
      then
        echo "Successfully copy to /root"
      else
        echo "Can't copy /root files!"
        exit 1
    fi
  else
    echo "Can't find /root. Creating..."
    cd "$DST_DIR"
    $CP -vr "$BS_DIR"/root .
    if [ $? == 0 ]
      then
        echo "Successfully create /root dir"
      else
        echo "Can't create /root!"
        exit 1
    fi
fi

}

mount_root ()
{
if [ -c "$DEV_ROOT" ]
  then
    if [ -n "$(mount | grep $DEV_ROOT)" ]
      then
        DST_DIR=$(mount |grep "$DEV_ROOT"|cut -d " " -f3)
        echo "Partition /root alredy mounted on $DST_DIR!"
      else
        $FSCK "$DEV_ROOT" > /dev/null
        $MOUNT "$DEV_ROOT" "$DST_DIR" > /dev/null
        echo "Partition /root has mounted on $DST_DIR."
      fi
  if [ -c "$DEV_ETC" ]
    then
      if [ -n "$(mount | grep $DEV_ETC)" ]
        then
          echo "Partition /etc alredy mounted on $DST_DIR/etc!"
        else
          $FSCK "$DEV_ETC" > /dev/null
          $MOUNT "$DEV_ETC" "$DST_DIR"/etc > /dev/null
          echo "Partition /etc has mounted on $DST_DIR/etc..."
        fi
    else
      echo "Can't find /etc partition!"
      exit 1
    fi
  else
    echo "Can't find /root partition!"
    exit 1
fi
}

umount_root ()
{
echo "Syncing disk..."
$SYNC
cd /
$UMOUNT  "$DEV_ETC";
if [ $? == 0 ]
  then
    echo "Umounting $DST_DIR/etc..."
  else
    echo "Can't umount $DST_DIR/etc!"
fi

$UMOUNT  "$DEV_ROOT";
if [ $? == 0 ]
  then
    echo "Umounting $DST_DIR..."
  else
    echo "Can't umount $DST_DIR!"
fi

}

get_platform_info ()
{
  $AWK 'NR > 1 {gsub(/\(|\)/, "") ;  print "This is " $1 " on hardware platform " $2 } ' "$DST_DIR"/etc/platform

}


make_symlinks ()
{
  if [ -d "$DST_DIR"/etc/rc.running ]
    then
      echo "Found rc.running dir. Making some fun now..."
      cd "$DST_DIR"/etc/rc.running
      if [ -e 01sh_0+-~ ]
        then
          echo "Symlink alredy exist. Nothing to do."
      else
        $LINKER -sn /bin/sh  01sh_0+-~
          if [ $? == 0 ]
            then
              echo "Successfully installing 01sh_0 symlink..."
            else
              echo "Can not install 01sh_0 symlink!"
          fi
      fi

      if [ -e 01sh_1+-~ ]
        then
          echo "Symlink alredy exist. Nothing to do."
        else
          $LINKER -sn /bin/sh  01sh_1+-~
            if [ $? == 0 ]
              then
                echo "Successfully installing 01sh_1 symlink..."
              else
                echo "Can not install 01sh_1 symlink!"
            fi
      fi

          if [ -e 07sshd+~ ]
        then
          echo "Symlink alredy exist. Nothing to do."
        else
          $LINKER -sn /bin/sshd  07sshd+~
            if [ $? == 0 ]
              then
                echo "Successfully installing 07sshd+~ symlink..."
              else
                echo "Can not install 07sshd+~ symlink!"
            fi
      fi

    else
      echo "Can't find $DST_DIR/etc/rc.running!"
      exit 1
  fi
}

remove_symlinks ()
{
if [ -d "$DST_DIR"/etc/rc.running ]
  then
    echo "Found rc.running dir. Making some fun now..."
    cd "$DST_DIR/etc/rc.running"
    if [ -e 01sh_0+-~ ]
      then
        $RM 01sh_0+-~
        if [ $? == 0 ]
          then
            echo "Successfully removing 01sh_0 symlink..."
          else
            echo "Can not remove 01sh_0 symlink!"
        fi
      else
        echo "Symlink 01sh_0 not found."
    fi
    if [ -e 01sh_1+-~ ]
      then
        $RM 01sh_1+-~
        if [ $? == 0 ]
          then
            echo "Successfully removing 01sh_1 symlink..."
          else
            echo "Can not remove 01sh_1 symlink!"
        fi
      else
        echo "Symlink 01sh_1 not found."
    fi

        if [ -e 07sshd+~ ]
      then
        $RM 07sshd+~
        if [ $? == 0 ]
          then
            echo "Successfully removing 07sshd+~ symlink..."
          else
            echo "Can not remove 07sshd+~ symlink!"
        fi
      else
        echo "Symlink 07sshd+~ not found."
    fi

        cd "$DST_DIR"

  else
    echo "Can't find rc.running!"
    exit 1
fi


}


case "$ACTION" in

## Install startup sh symlinks ##
  -i)
    echo "This script will install SSH daemon on port 22 and create symlinks to run Bourn Shell on vtty03 and vtty04 on boot."
    read -p "Press any key to continue..." $ASK
    mount_root
    get_platform_info
    bootstrap
    make_symlinks
    umount_root
  ;;
## Remove sh symlinks ##
  -u)
    echo "This script will remove SSH daemon and symlinks to run Bourn Shell on vtty03 and vtty04 on boot"
    read -p "Press any key to continue..." $ASK
    mount_root
    get_platform_info
    remove_symlinks
    umount_root

  ;;

  *)
    echo -e "\nContinent Service Depatrment Shell"
    echo "usage:"
    echo "      $(basename ${0}) -i:    Install shell, symlinks and tools"
    echo "      $(basename ${0}) -u:    Uninstall shell, symlinks and tools"
    exit 1
  ;;
esac

